def turbulance(y: np.ndarray, historical_returns: np.ndarray) -> np.ndarray:
    """ Function models turbulance given period returns and historical returns.

    Parameters
    ----------
    y : np.ndarray
        Vector of returns in given period. Length equals to number of instruments.
    historical_returns : np.ndarray
        Matrix of historical returns. Shape ~ (n_days_history, n_instruments).

    Returns
    -------
    float
        Value of turbulence.
    """

    # (y - mu)
    first_factor = y - historical_returns.mean(0)

    # inverse sigma
    second_factor = np.linalg.inv(np.cov(historical_returns))

    return first_factor @ second_factor @ first_factor.T