#region imports
from AlgorithmImports import *
import numpy as np

# https://quantpedia.com/Screener/Details/25
class SmallCapInvestmentAlgorithm(QCAlgorithm):

    def Initialize(self):

        self.SetStartDate(2020, 1, 1)
        self.SetEndDate(2021, 3, 1)
        self.SetCash(100000)

        self.count = 10

        self.vixDays = 25
        self.VixJump = 0.1

        self.SetWarmUp(25)
        self.vix = self.AddData(CBOE, 'VIX', Resolution.Daily).Symbol
        #historical 25 day vix
        self.vix_sma = self.SMA(self.vix, self.vixDays, Resolution.Daily)
        self.vix_close = self.SMA(self.vix, 1, Resolution.Daily)
        self.vix_sma_multiplication = IndicatorExtensions.Times(self.vix_sma, (1+ self.VixJump))

        self.UniverseSettings.Resolution = Resolution.Daily
        self.AddUniverse(self.CoarseSelectionFunction, self.FineSelectionFunction)
        self.SetBenchmark("SPY")

    def CoarseSelectionFunction(self, coarse):
        ''' Drop stocks which have no fundamental data or have low price '''
        return [x.Symbol for x in coarse if x.HasFundamentalData and x.Price > 5]
 
 
    def FineSelectionFunction(self, fine): #small cap stocks
        ''' Selects the stocks by lowest market cap '''
        sorted_market_cap = sorted([x for x in fine if x.MarketCap > 1000000000],
            key=lambda x: x.MarketCap)
        return [x.Symbol for x in sorted_market_cap[:self.count]]


    def OnData(self, data):

        self.Log(self.vix_sma)
        self.Log(self.vix_close)
        self.Log(self.vix_sma_multiplication)

        if not self.IsWarmingUp and not self.Portfolio.Invested:
            if self.vix_sma_multiplication > self.vix_close:
                for symbol in self.ActiveSecurities.Keys:
                    self.SetHoldings(symbol, 1/self.count)
            if self.Portfolio.Invested:
                if self.vix_sma_multiplication > self.vix_close:
                    return
                if self.vix_sma_multiplication < self.vix_close:
                    self.Liquidate()
            
    def OnSecuritiesChanged(self, changes):
        ''' Liquidate the securities that were removed from the universe '''
        for security in changes.RemovedSecurities:
            symbol = security.Symbol
            if self.Portfolio[symbol].Invested:
                self.Liquidate(symbol, 'Removed from Universe')